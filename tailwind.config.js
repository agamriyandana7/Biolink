const colors = require('tailwindcss/colors')
/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["index.html",
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    'primary': '#3490dc',
    'secondary': '#ffed4a',
    'danger': '#e3342f',
    fontFamily: {
    sans: ['Graphik', 'sans-serif'],
    serif: ['Merriweather', 'serif'],
    },
   extend: {},
  },
  plugins: [
    require('postcss-import'),
    require('tailwindcss/nesting': 'postcss-nesting'),
    require('tailwindcss'),
    require('autoprefixer'),
  ]
}
